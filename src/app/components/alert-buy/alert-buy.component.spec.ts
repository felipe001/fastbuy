import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertBuyComponent } from './alert-buy.component';

describe('AlertBuyComponent', () => {
  let component: AlertBuyComponent;
  let fixture: ComponentFixture<AlertBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertBuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
