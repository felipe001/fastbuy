import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { ModalCardComponent } from './modal-card/modal-card.component';
import { ModalInfoComponent } from './modal-info/modal-info.component';
import { AlertBuyComponent } from './alert-buy/alert-buy.component';
import { CarruselComponent } from './carrusel/carrusel.component';



@NgModule({
  declarations: [
  	NavbarComponent,
  	HeaderComponent,
  	ModalCardComponent,
  	ModalInfoComponent,
  	AlertBuyComponent,
  	CarruselComponent
  ],
  exports: [
  	NavbarComponent,
    HeaderComponent,
    ModalCardComponent,
    ModalInfoComponent,
    AlertBuyComponent,
    CarruselComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class ComponentsModule { }
