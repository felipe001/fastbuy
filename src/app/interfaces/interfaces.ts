export interface Tienda {
	id: string;
	name: string;
	owner: string;
}