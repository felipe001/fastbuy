import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { Tienda } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  constructor( private http: HttpClient ) {   }

  getTiendas(){
  		return this.http.get<Tienda[]>(`${ environment.url }/api/fastbuy-dev/tiendas`)			
  }

}
