import { Component, OnInit } from '@angular/core';
import { TiendaService } from '../../services/tienda.service';
import { Tienda } from '../../interfaces/interfaces';

@Component({
  selector: 'app-explorar',
  templateUrl: './explorar.component.html',
  styleUrls: ['./explorar.component.css']
})
export class ExplorarComponent implements OnInit {

tiendas: Tienda[] = [];

constructor( private tiendaService: TiendaService) { }

ngOnInit() {

	this.tiendaService.getTiendas()
		.subscribe( resp => {
			console.log(resp);
		} )

}

}
