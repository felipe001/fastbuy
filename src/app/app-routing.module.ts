import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { ExplorarComponent } from './pages/explorar/explorar.component';

const routes: Routes = [
	{ path: 'inicio', component:InicioComponent },
	{ path: 'explorar', component:ExplorarComponent },
	{ path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
